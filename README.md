# Многофункциональный закипатель воды в чайнике

Наша компания с гордостью представляет обьект `cook` (повар), который способен выполнить любые операции по Вашему желанию (если они ограничиваются необходимостью закипятить воду в чайнике =)).

Простой и неказистый обьект `cook` обладает целыми тремя способностями, которые Вы можете использовать, как угодно. Эти способности реализованы посредством следующих методов.

## Метод `cook.waterTheTeapot()`

С помощью этого метода повр наполняет чайник водой. Той самой, которую необходимо вскипятить.

## Метод `cook.placeOnStove()`

Так можно заставить повара расположить чайник на плите.

## Метод `cook.igniteFire(x)`

Заставляет повара зажечь огонь и подождать `x` минут. В идеале столько, чтобы вода вскипела. Не перестарайтесь и не задавайте слишком маленькое время. Повар довольно глуп и будет ждать ровно сколько вы ему скажете, не смотря вскипела ли вода или не сгорел ли чайник.
