(function(window, document) {
  TEAPOT_STATES = [
    "start",
    "watered",
    "on-stove",
    "heating-complete",
    "over-heated"
  ]

  function Cook(teapotImage, statusDiv) {
    this.state = "start"
    this.waterTheTeapot = function() {
      if (this.state != "start") {
        statusDiv.innerHTML = "Без толку заливать воду уже!"
        return
      }

      this.state = "watered"
      statusDiv.innerHTML = "Водичку залили. Может поставить на плиту стоит?"
    }

    this.placeOnStove = function() {
      if (this.state == "start") {
        statusDiv.innerHTML = "Воды в чайнике нет. Наполнить нужно сначала!"
        return
      }

      if (this.state == "watered") {
        statusDiv.innerHTML = "Окей, надо зажечь огонь"
        teapotImage.src = "teapot-no-gas.jpg"
        this.state = "on-stove"
        return
      }

      statusDiv.innerHTML = "Уже на плите"
    }

    this.igniteFire = function(waitTime) {
      if (this.state == "start") { 
        statusDiv.innerHTML = "Рано зажигать! Надо налить воды сперва!"
        return
      }

      if (this.state == "watered") {
        statusDiv.innerHTML = "Еще не на плите чайник! Надо поставить на плиту!"
        return
      }

      if (this.state == "on-stove") {
        if (waitTime >= 5 && waitTime <= 10) {
          statusDiv.innerHTML = "Чайник успешно вскипел!"
          teapotImage.src = "teapot-steam.jpg"
          this.status = "heating-complete"
        } else if (waitTime < 5) {
          statusDiv.innerHTML = "Недостаточно времени! Вода не вскипела."
          teapotImage.src = "teapot-no-gas.jpg"
          this.status = "on-stove"
        } else {
          statusDiv.innerHTML = "Перекипел!"
          teapotImage.src = "teapot-burnt.jpg"
          this.status = "over-heated"
        }

        return
      }

      statusDiv.innerHTML = "Уже поздно поджигать =). Попробуйте снова =)"
    }
  }

  window.Cook = Cook
})(window, document)
